package com.example.trafficlight;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.FileObserver;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private ConstraintLayout mConstraintLayout;
    Button redButton,yellowButton,greenButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mConstraintLayout = (ConstraintLayout)findViewById(R.id.mConstraintLayout);
        redButton = (Button)findViewById(R.id.redLight);
        yellowButton = (Button)findViewById(R.id.yellowLight);
        greenButton = (Button)findViewById(R.id.greenLight);
        buttonClick();
    }
    void buttonClick(){
        redButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mConstraintLayout.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorRed));
            }
        });

        yellowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mConstraintLayout.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorYellow));
            }
        });

        greenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mConstraintLayout.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorGreen));
            }
        });
    }
    }

